import React from 'react';
import axios from 'axios';

import StudentList from './student-list'
 
export default class ReactApp extends React.Component {
 
    constructor(props) {
        super(props);
        this.state = {students: []};
        this.Axios = axios.create({
            baseURL: "https://api.airtable.com/v0/appLNbQOB0WL1ZeDD/Table%201?view=Grid%20view",
            headers: {'Authorization': 'Bearer keyGNH2xgTYp82F5o'}
        });
    }
 
    componentDidMount() {
        let _this = this;
        this.Axios.get()
          .then(function (response) {
             console.log(response);
            _this.setState({students: response.data.records});
          })
          .catch(function (error) {
            console.log(error);
          });
    }
 
    render() {
        return (
                <div>
                  <StudentList students={this.state.students}/>
                </div>
            )
    }
}