import React from 'react';
import Student from './student'
 
export default class StudentList extends React.Component{
     
    render() {
        var students = this.props.students.map((student, i) =>
            <Student key={i} student={student}/>
        );
         
        return (
            <table>
                <tbody>
                    <tr>
                        <th>姓名</th>
                        <th>年齡</th>
                        <th>城市名稱</th>
                    </tr>
                    {students}
                </tbody>
            </table>
        )
    }
}