import React from 'react';
 
export default class Student extends React.Component{
    render() {
        return (
            <tr>
                <td>{this.props.student.fields.Name}</td>
                <td>{this.props.student.fields.Age}</td>
                <td>{this.props.student.fields.City}</td>
            </tr>
        )
    }
}